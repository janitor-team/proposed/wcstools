.help sky2xy Mar2001 WCSTools
.ih
NAME
sky2xy \- Compute X Y from RA Dec using WCS in FITS and IRAF image files
.ih
USAGE
sky2xy [-vbjg] file.fts ra1 dec1 ... ran decn
.br
or
.br
sky2xy [-vbjg] file.fts @listfile
.ih
ARGUMENTS
.ls -b
B1950 (FK4) input
.le
.ls -3
Ecliptic longitude and latitude input
.le
.ls -j
J2000 (FK5) input
.le
.ls -g
Galactic longitude and latitude input
.le
.ls -v
More descriptive output
.le
.ih
DESCRIPTION
.I sky2xy
(pronounced sky-to-X-Y) is a utility for getting the image coordinates of
a set of world coordinates in a FITS or IRAF image. World coordinate
system (WCS) information in the image header describes, in a standard
way, the relationship between sky coordinates and image pixels. This
information is used by the program to transform image right ascension
and declination sky coordinate pairs to image coordinate (X,Y) pairs.
The program prints the coordinates at the same system as is given in the
image header, but future versions will allow transformations to B1950/FK4,
J2000/FK5, ecliptic or galactic coordinates. Input and output files in the
Starbase format will eventually be allowed. 
.ih
SEE ALSO
xy2sky
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/xy2sky.html
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
