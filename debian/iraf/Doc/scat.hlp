.help scat Sep2001 WCSTools
.ih
NAME
scat -- Find catalog stars in a square or circle on the sky
.ih
USAGE
scat [options] [-b or -j] ra dec
.ih
ARGUMENTS
.ls -a
List the single closest catalog source
.le
.ls -b <RA> <Dec>
Output B1950 (FK4) coordinates around this center
.le
.ls -c <name>
Reference catalog (gsc, uac, ujc, or tab table file
.le
.ls -d
Sort by distance from center instead of flux
.le
.ls -g <class>
HST Guide Star Catalog object class (0=stars 3=galaxies -1=all)
.le
.ls -h
Print search and column heading, else do not 
.le
.ls -j <RA> <Dec>
Output J2000 (FK5) coordinates around this center
.le
.ls -k <keyword>
Add value of this keyword to end of output lines from a tab table search
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -o <name>
Object name used for output file naming
.le
.ls -r <radius>
Halfwidth (-radius if negative) of search area in arcseconds (default 10)
.le
.ls -s
Sort by right ascension instead of flux 
.le
.ls -t
Tab table to standard output as well as file
.le
.ls -u <num>
USNO catalog single plate number to accept
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file objectname.catalog
.le
.ih
DESCRIPTION
.I scat
is a utility for finding all of the Hubble Space Telescope Guide Star
Catalog (GSC), GSC-ACT (GSCA), GSC II (GSC2),
U.S. Naval Observatory A2.0 (UA2), SA2.0 (USA2), or UJ (UJC)
Catalogs, or locally catalogued objects in a specified region of the sky
and listing their sky coordinates to standard output. If the -w flag is
set, they are written to a file objectname.catalogname, where objectname
is given by the argument to -o on the command line, defaulting to "search",
and catalogname is given by the argument to -c on the command line. The
catalog defaults to the GSC, if no -c argument is present. scat resembles
rgsc(1) and star(1), which can search from lists of coordinates but
cannot sort the output.  sgsc, sgsca, sgsc2, stmc, sua2, and susa2 are
links to this program.
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/scat/
.ih
SEE ALSO
sgsc, sgsc2, sgsca, stmc, sua2, susa2
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
