.help imstar Mar2001 WCSTools
.ih
NAME
imstar -- Find stars in FITS and IRAF image files
.ih
USAGE
imstar  [options] [FITS or IRAF filename] or [@file of image file names]
.ih
ARGUMENTS
.ls <hh:mm:ss> <dd:mm:ss> [J2000, B1950]
Coordinates for center (or reference pixel if -x is used).
.le
.ls -a <angle>
Image rotation angle in degrees (default 0).  If multiple of 90, rotate
image before search and set WCS angle to zero; if not, put in WCS.
.le
.ls -b
Output B1950 (FK4) coordinates (default=image equinox)
.le
.ls -d <catalog name>
Read this DAOFIND style catalog of X, Y, and magnitude instead of searching
for stars in the image.  The format is simply white-space-separated numbers
on a line, with # at the beginning of comment lines.
.le
.ls -e <num>
Number of pixels to ignore around image edge 
.le
.ls -f
Write a simple ASCII catalog file instead of tab table or DAOFIND format
(number RA DEC mag ... per line, with two lines of header info)
.le
.ls -h
Print heading, else do not 
.le
.ls -i <num>
Minimum peak value for star in image (<0=-sigma)
Setting this to reject all but 10-15 stars is a good way to speed up the
star-finding process.  If num is less than zero, the minimum peak is -num
image pixel standard deviations.  Setting this number rejects faint stars
early in the selection process for a significant saving in computing time.
.le
.ls -j
Output J2000 (FK5) coordinates (default=image equinox)
.le
.ls -k
Print each star as it is found for debugging 
.le
.ls -l
Reflect the image left <-> right before rotating (-a) and searching for stars.
.le
.ls -m <magnitude>
Magnitude offset
.le
.ls -n
Number of brightest stars to print 
.le
.ls -o
Output star list in DAOFIND format.
The first three numbers on each line of the
output file are X, Y, and magnitude, separated by one or more blanks or
a tab.  Lines beginning with # are ignored.
.le
.ls -p <num>
Plate scale in arcsec per pixel (default 0)
.le
.ls -q <c|d|o|s|x|v|+>
Output region file shape for SAOimage (default o)
Characters mean: c>ross, d>iamond, s>quare, o>circle, x=X, v>ary with GSC
type, +>cross.
.le
.ls -r
Maximum radius for star in pixels 
.le
.ls -s
Sort by RA instead of flux 
.le
.ls -t
Output in Starbase tab table format
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write output to a file in addition to standard out.
If DAO format (-o), create the name by adding .dao to the image file name.
If Starbase format (-t), create the name by adding .tab to the image file name.
If ASCII format (-f), create the name by adding .stars to the image file name.
.le
.ls -x <X> <Y>
X and Y coordinates of reference pixel (if not in header or image center)
.le
.ls -z
Use AIPS classic projection code (for "-SIN", "-TAN", "-ARC", "-NCP",
"-GLS", "-MER", "-AIT" and "-STG" only) instead of WCSLIB proposed
standard projection code.
.le
.ih
DESCRIPTION
.I
imstar
is a utility for finding all of the stars in a FITS or IRAF image and
listing their sky positions if a world coordinate system is present.
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/imstar/
.ihh
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
