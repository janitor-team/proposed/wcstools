.help gethead Mar2001 WCSTools
.ih
NAME
gethead -- Print FITS or IRAF header keyword values
.ih
USAGE
gethead [-hptv] [-d pathname] [-n num] <FITS or IRAF file> kw1 kw2 ... kwn
.ih
DESCRIPTION
Print values of the specified keywords from the given image header.  By
default they are all listed on one line, separated by spaces.  The
.I \-v
flag causes the keyword names and values to be printed, one keyword per
line.
To read keywords from a list of files, substitute @<listfile> for the
file names on the command line.  To read a lot of keywords, put them,
one per line, in a file and substitute @<keylistfile> on the command line.
If two @ commands are present, the program will figure out which contains
file names and which contains keywords.
.ih
ARGUMENTS
.ls -a
List file name even if keywords are not found
.le
.ls -d
Root directory for input files (default is cwd)
.le
.ls -f
Never print filenames (default is to print them if more than one)
.le
.ls -h
flag causes the keyword names to be printed at top of columns.
.le
.ls -n
Number of decimal places in numeric output
.le
.ls -p
Print full pathnames of files
.le
.ls -t
flag causes the output to be in tab-separated tables with keyword column
headings.
.le
.ls -u
Always print ___ if keyword not found, event if only one keyword in search
.le
.ls -v
Print output as <keyword>=<value>, one per line
.le
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/gethead.html
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
