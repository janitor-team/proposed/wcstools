.help imrot Mar2001 WCSTools
.ih
NAME
imrot -- Rotate and/or Reflect FITS and IRAF image files
.ih
USAGE
imrot [options] Fits or IRAF file(s)
.ih
DESCRIPTION
.I imrot
is a utility for rotating and/or reflecting FITS or IRAF images.  Images can
be rotated only in multiples of 90 degrees.  The image may be output with
a different data type than that in which it is input, and IRAF files may be
written out as FITS files.
.ih
ARGUMENTS
.ls -f
Write FITS image from IRAF input
.le
.ls -l
Reflect image across vertical axis
.le
.ls -o
Allow overwriting of input image, else write new one
.le
.ls -r <angle>
Image rotation angle in degrees (default 0)
.le
.ls -v
Verbose
.le
.ls -x <num>
Output pixel size in bits (FITS code, default=input)
Allowed values are 8, 16, 32 for integers, -16 for unsigned integers, and
-32 and -64 for 4- and 8-byte floating point numbers
.le
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/imrot.html
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
