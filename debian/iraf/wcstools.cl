#{ WCSTools functions accessable from IRAF

package wcstools

set     wcsbin		= "/usr/bin/"

task	$addpix		= "$"//osfn("wcsbin$addpix")
task	$conpix		= "$"//osfn("wcsbin$conpix")
task	$chead		= "$"//osfn("wcsbin$cphead")
task	$dhead		= "$"//osfn("wcsbin$delhead")
task	$dwcs		= "$"//osfn("wcsbin$delwcs")
task	$edhead		= "$"//osfn("wcsbin$edhead")
task	$fname		= "$"//osfn("wcsbin$filename")
task	$froot		= "$"//osfn("wcsbin$fileroot")
task	$filext		= "$"//osfn("wcsbin$filext")
task	$getcol		= "$"//osfn("wcsbin$getcol")
task	$getdate	= "$"//osfn("wcsbin$getdate")
task	$getfits	= "$"//osfn("wcsbin$getfits")
task	$gethead	= "$"//osfn("wcsbin$gethead")
task	$getpix		= "$"//osfn("wcsbin$getpix")
task	$gettab		= "$"//osfn("wcsbin$gettab")
task	$i2f		= "$"//osfn("wcsbin$i2f")
task	$ihead		= "$"//osfn("wcsbin$imhead")
task	$imcat		= "$"//osfn("wcsbin$imcatalog")
task	$imxtract	= "$"//osfn("wcsbin$imextract")
task	$imextract	= "$"//osfn("wcsbin$imextract")
task	$imfill		= "$"//osfn("wcsbin$imfill")
task	$immcat		= "$"//osfn("wcsbin$immatch")
task	$imgsc		= "$"//osfn("wcsbin$imcatalog")//" -c gsc"
task	$imgsc2		= "$"//osfn("wcsbin$imcatalog")//" -c gsc2"
task	$imgsca		= "$"//osfn("wcsbin$imcatalog")//" -c gscat"
task	$imub1		= "$"//osfn("wcsbin$imcatalog")//" -c ub1"
task	$imua2		= "$"//osfn("wcsbin$imcatalog")//" -c ua2"
task	$imusa2		= "$"//osfn("wcsbin$imcatalog")//" -c usa2"
task	$imucac2	= "$"//osfn("wcsbin$imcatalog")//" -c ucac2"
task	$imucac3	= "$"//osfn("wcsbin$imcatalog")//" -c ucac3"
task	$imsdss		= "$"//osfn("wcsbin$imcatalog")//" -c sdss"
task	$imppm		= "$"//osfn("wcsbin$imcatalog")//" -c ppm"
task	$imtmc		= "$"//osfn("wcsbin$imcatalog")//" -c tmc"
task	$imsao		= "$"//osfn("wcsbin$imcatalog")//" -c sao"
task	$imresize	= "$"//osfn("wcsbin$imresize")
task	$imrot		= "$"//osfn("wcsbin$imrot")
task	$imsize		= "$"//osfn("wcsbin$imsize")
task	$imsmooth	= "$"//osfn("wcsbin$imsmooth")
task	$imstar		= "$"//osfn("wcsbin$imstar")
task	$imwcs		= "$"//osfn("wcsbin$imwcs")
task	$imwgsc		= "$"//osfn("wcsbin$imwcs")//" -c gsc"
task	$imwgsca	= "$"//osfn("wcsbin$imwcs")//" -c gscat"
task	$imwgsc2	= "$"//osfn("wcsbin$imwcs")//" -c gsc2"
task	$imwppm		= "$"//osfn("wcsbin$imwcs")//" -c ppm"
task	$imwsao		= "$"//osfn("wcsbin$imwcs")//" -c sao"
task	$imwtmc		= "$"//osfn("wcsbin$imwcs")//" -c tmc"
task	$imwty2		= "$"//osfn("wcsbin$imwcs")//" -c ty2"
task	$imwua2		= "$"//osfn("wcsbin$imwcs")//" -c ua2"
task	$imwub1		= "$"//osfn("wcsbin$imwcs")//" -c ub1"
task	$istack		= "$"//osfn("wcsbin$imstack")
task	$keyhead	= "$"//osfn("wcsbin$keyhead")
task	$newfits	= "$"//osfn("wcsbin$newfits")
task	$remap		= "$"//osfn("wcsbin$wcsremap")
task	$scat		= "$"//osfn("wcsbin$scat")
task	$sgsc		= "$"//osfn("wcsbin$scat")//" -c gsc"
task	$sgsc2		= "$"//osfn("wcsbin$scat")//" -c gsc2"
task	$sgsca		= "$"//osfn("wcsbin$scat")//" -c gscat"
task	$sppm		= "$"//osfn("wcsbin$scat")//" -c ppm"
task	$ssao		= "$"//osfn("wcsbin$scat")//" -c sao"
task	$stmc		= "$"//osfn("wcsbin$scat")//" -c tmc"
task	$sub1		= "$"//osfn("wcsbin$scat")//" -c ub1"
task	$sua2		= "$"//osfn("wcsbin$scat")//" -c ua2"
task	$susa2		= "$"//osfn("wcsbin$scat")//" -c usa2"
task	$sucac2		= "$"//osfn("wcsbin$scat")//" -c ucac2"
task	$sucac3		= "$"//osfn("wcsbin$scat")//" -c ucac3"
task	$ssdss		= "$"//osfn("wcsbin$scat")//" -c sdss"
task	$sethead	= "$"//osfn("wcsbin$sethead")
task	$setpix		= "$"//osfn("wcsbin$setpix")
task	$simpos		= "$"//osfn("wcsbin$simpos")
task	$sky2xy		= "$"//osfn("wcsbin$sky2xy")
task	$skycoor	= "$"//osfn("wcsbin$skycoor")
task	$sumpix		= "$"//osfn("wcsbin$sumpix")
task	$wcshead	= "$"//osfn("wcsbin$wcshead")
task	$xy2sky		= "$"//osfn("wcsbin$xy2sky")

# Write the welcome message
if (motd)
    type wcstools$wcstools.msg
;

clbye()
