wcstools (3.9.6-1) unstable; urgency=low

  * Fix defdir in helpdb.mip
  * Use http download URL (https does not work yet)
  * Add gitlab-ci file to trigger automated CI test
  * New upstream version 3.9.6. Rediff patches
  * Push Standards-Version to 4.5.0. No changes needed.
  * Push dh-compat to 13
  * Fix getpix output
  * Fix axes order in getpix test
  * Update symbols file
  * Add Rules-Requires-Root: no to d/control
  * Replace ADTTMP with AUTOPKG_TMP

 -- Ole Streicher <olebole@debian.org>  Thu, 25 Jun 2020 15:43:46 +0200

wcstools (3.9.5-3) unstable; urgency=medium

  [ Ole Streicher ]
  * Add Enhances: iraf to d/control
  * Push Standards-Version to 4.1.4. Use https in copyright format URL
  * Push compat to 11

  [ Helmut Grohne ]
  * wcstools FTCBFS: fails running tests despite DEB_BUILD_OPTIONS=nocheck
    (Closes: Bug#899016)

 -- Ole Streicher <olebole@debian.org>  Fri, 18 May 2018 21:22:48 +0200

wcstools (3.9.5-2) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * Add IRAF package for wcstools
  * Set -dev package to Multi-Arch: same

 -- Ole Streicher <olebole@debian.org>  Tue, 23 Jan 2018 21:30:58 +0100

wcstools (3.9.5-1) unstable; urgency=low

  * New upstream version 3.9.5
  * Rediff patches
  * Update Standards-Version to 4.0.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Mon, 19 Jun 2017 18:05:36 +0200

wcstools (3.9.4-2) unstable; urgency=low

  * Switch back to unstable

 -- Ole Streicher <olebole@debian.org>  Thu, 11 Aug 2016 21:36:39 +0200

wcstools (3.9.4-2~exp1) experimental; urgency=low

  * Start a transition (new SONAME). Closes: #833924

 -- Ole Streicher <olebole@debian.org>  Wed, 10 Aug 2016 14:34:56 +0200

wcstools (3.9.4-1) unstable; urgency=low

  * Add ASCL-Id to d/u/metadata
  * Update Standards-Version to 3.9.8. No changes needed
  * Imported Upstream version 3.9.4

 -- Ole Streicher <olebole@debian.org>  Fri, 05 Aug 2016 16:35:20 +0200

wcstools (3.9.2-5) unstable; urgency=low

  * Another workaround for long CTYPEs.
  * Update standards-version to 3.9.7. No changes needed.
  * Update VCS fields in d/control.

 -- Ole Streicher <olebole@debian.org>  Sat, 13 Feb 2016 21:44:58 +0100

wcstools (3.9.2-4) unstable; urgency=low

  * Fix crash and undefined behaviour with imstar
  * Use -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 for large file support

 -- Ole Streicher <olebole@debian.org>  Fri, 12 Jun 2015 14:18:26 +0200

wcstools (3.9.2-3) unstable; urgency=low

  * Run CI test during package build as well
  * Revert last fix (not working), and replace with workaround

 -- Ole Streicher <olebole@debian.org>  Mon, 01 Jun 2015 17:18:46 +0200

wcstools (3.9.2-2) unstable; urgency=low

  * Fix crash with long CTYPEs. LP: #1458333

 -- Ole Streicher <olebole@debian.org>  Wed, 27 May 2015 10:44:10 +0200

wcstools (3.9.2-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Sat, 16 May 2015 14:32:55 +0200

wcstools (3.9.1-1) unstable; urgency=low

  * New upstream version
  * Update standards version to 3.9.6. No changes needed.
  * Create upstream/metadata with bibliographic information
  * Create symbols file
  * Create CI tests

 -- Ole Streicher <olebole@debian.org>  Sat, 25 Apr 2015 13:21:41 +0200

wcstools (3.9.0+dfsg-1) unstable; urgency=low

  * New upstream version
  * Change maintainer and VCS location to debian-astro
  * Change uploader's e-mail address.

 -- Ole Streicher <olebole@debian.org>  Tue, 02 Sep 2014 11:15:07 +0200

wcstools (3.8.7-2) unstable; urgency=low

  * Install wcscat.h. Closes: #717942
  * Fix crashes found by the mayhem tool. Closes: #715682, #715755,
    #715905, #715956, #715957

 -- Ole Streicher <debian@liska.ath.cx>  Sat, 27 Jul 2013 17:24:33 +0200

wcstools (3.8.7-1) unstable; urgency=low

  * Package description corrected. Closes: #699683
  * Push policy version to 3.9.4 (no changes)
  * Forward CFLAGS, CPPFLAGS, LDFLAGS for fortification

 -- Ole Streicher <debian@liska.ath.cx>  Sat, 20 Apr 2013 08:33:51 +0200

wcstools (3.8.7-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Sun, 21 Oct 2012 13:48:34 +0200

wcstools (3.8.6-1~exp1) experimental; urgency=low

  * New upstream version
  * Multiarch support
  * Fortify compilation

 -- Ole Streicher <debian@liska.ath.cx>  Thu, 16 Aug 2012 15:07:34 +0200

wcstools (3.8.5-1) unstable; urgency=low

  * New upstream version.
  * Bump Standards version to 3.9.3.
  * Set DM-Upload-Allowed: yes
  * Fix a crash that appears in saods9

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 04 Jun 2012 08:30:00 +0200

wcstools (3.8.4-2) unstable; urgency=low

  * Rename remap to wcsremap. Closes: #657644
  * Rename imcat to imcatalog. Closes: #657645

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 27 Jan 2012 20:26:17 +0100

wcstools (3.8.4-1) unstable; urgency=low

  * Initial release. (Closes: #655695)

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 13 Jan 2012 17:11:08 +0100
